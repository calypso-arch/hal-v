# HAL-V - A RISC-V Pipelined Out of Order Execution CPU

Open the pod bay doors HAL!

## Architecture Goals

* Pipelined Design
* Out of Order Execution
* Priviliedge Modes
* Cache
* Superscalar

## Memory Architecture

* [L1 Cache Design](/l1-cahce.md)