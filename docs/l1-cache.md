# L1 Cache

## L1 Cache Features

* 48-bit Virtual address space
* Split cache
* Non-Blocking cache supporting hit under miss
* Merged write buffer
* Critical word first
* 64KiB L1I cache and 64KiB L1D cache - 128KiB L1 cache
* 8-way set associative L1 cache
* 64 byte block size
* Physically indexed and virtually tagged L1 cache addressing

##L1 Cache Modules

* Standard (Wishbone/AXI/AMBA) vs Proprietary Bus
* CPU - L1D Cache Write Buffer
* Hit/Miss Checker
* Translation Lookaside Buffer (TLB)
* TLB Addressing Logic -- Probably included with BRAM IP
* TLB Miss Handler
* Cache Addressing Logic -- Probably included with BRAM IP
* Cache Way - incl. Hit/Miss Checker
* Cache Data Read/Write Unit
* Cache Miss Handler
* Cache Line Eviction Unit
* Cache Line Return Handler
* Pseudo-LRU Processor
* On-demand Cache Line Write-through and Invalidation Logic
* L1D - L2 Cache Write Buffer








