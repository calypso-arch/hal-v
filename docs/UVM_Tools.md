# UVM Verification Tools
## Verilator
Free and open source  tool
Converts verilog to a cycle-accurate behavioural model in C++ and SystemC

Verilator can handle all versions of verilog, systemverilog, sugar/PSL assertions

Does not work with VHDL

### Installation
`sudo apt install verilator` <-- This version will **NOT** work with cocotb; follow the git-clone procedure

### Usage
`verilator -Wall -cc <name of the file>`

### Limitations
- Can only design components for which you have all verilog logic
- limited support for tri-state busses
- No support for vendor specific clock modules or SERDES capabilities

tri-state busses - an integrated circuit that connects multiple data sources to a single bus

- No support for handling 'x' - unknown values or 'z' - high impedence values
- Does not recognise comments
- synthesizing a project with verilator will provide no feedback regarding weather or not your design will fit within a particular device

Verilator - recommended for young engineers trying to learn verilog for the first time

ZIPCPU - uses verilator for almost all his projects

## Cocotb
- Gaining traction
- Excellent verification tool
- Easier to be productive

- Downside - Simulation performance ] -> not a big issue
- Comparitively cocotb testbench is 2.4X slower to other UVM's
- Compared to python and verilator to create test vector cocotb is a huge improvement
-Works best with Icarus verilog, but can work with multiple simulators (like verilator)

### Installation
- Install a simulator (Icarus verilog preffered) `sudo apt-get install iverilog`
- Install pre-requisites `sudo apt-get install make gcc g++ python3 python3-dev python3-pip`
- Install the tool itself `pip install cocotb`

*Follow [this](https://docs.cocotb.org/en/stable/install.html) link for further information*

### Usage
- Go thrugh the website documentation for proper user guide, but basically requires the presence of 3 files:-
	- The verilog design file
	- The Python testbench
	- the makefile

- With those three files in place all you have to do to run the testbench is type `make` and press enter


## SymbiYOSYS
can be used with or without verific
verific needs to be purchased
- verific has some faults in formal verification

SymbiYOSYS needs to be purchased in order to get access to all systemverilog features

There is an open source version of symbiYOSYS

## Reference
verilator - https://zipcpu.com/blog/2017/06/21/looking-at-verilator.html
symbiYosys - https://www.reddit.com/r/FPGA/comments/cs4kzu/formal_verification_symbiyosys_limitations/
cocotb - https://www.reddit.com/r/FPGA/comments/klif9b/how_useful_has_cocotb_been_for_you/?utm_source=share&utm_medium=web2x&context=3

